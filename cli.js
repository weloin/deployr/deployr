#!/usr/bin/env node

import { dirname } from 'path';
import { fileURLToPath } from 'url';

import "./helpers/overriders.js";
import App from "./core/App.js";
import env from "./helpers/env.js";
import log from "./helpers/log.js";
import config from "./config.js";

global.APP_ROOT = dirname(fileURLToPath(import.meta.url));
global.APP_ENV = (process.env.APP_ENV || 'prod').split(',');

try{


    log.debug('DEPLOYR:APP:DIR', config.dirs.app);

    /**
     * Load Configurations based on the current environment set.
     */

    await env.merge(global.APP_ENV);

    /**
     * Initialize the Application and start.
     * @type {any}
     */
    const app = App.make();
    await app.start();

}catch (err) {
    log.critical('DEPLOYR:FAILED', err);
}
