# Deployr

Deployr is a stand-alone deployer to services like AWS,
Azure, GCP, Digital Ocean, etc. It automates the process
of deployment and provides easy CLI interface. It can
also be installed as a library within your application
to programmatically control deployments.

---

First, Install deployr globally from local package if not using npmjs package.

```shell
npm link
```


## CLI

The CLI provides a developer to quickly deploy their
application to cloud hosting services.

Below are the list of available commands:

### Deploy to AWS

```shell
deployr aws:up [command]
    --region|r=us-east-1  AWS Region Mame
    --zone|z              AWS Zone
    --instance|i          AWS Instance ID
    --remote-path|p       Server's remote path
    --number|n            Number of instances to start.
    --forever|pm2         Start your application with pm2 service.
```

### List AWS Regions

```shell
deployr aws:regions
    --available   List regions only that are available within your account.
```
