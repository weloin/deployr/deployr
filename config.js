export default {

    dirs: {
        root: '/',
        core: 'core',
        helpers: 'helpers',
        commands: 'commands',
        web: 'web',

        app: process.cwd(),
        storage: `${process.cwd()}/deployr/storage`,
    },


    app: {
        name: 'APP',
        group: 'DEPLOYR',
        start_script: 'index.js',
    },

    log: {
        level: 'INFO',
        output: 'app.out.log',
        error: 'app.err.log',
    },

    excludes: [
        '.idea',
        '.git',
        'node_modules',
        'deployr',
    ],

    deploy: {

        aws: {
            api_version: 'latest',

            api_key: '',
            api_secret: '',

            ready_wait: 5000,

            default_region: 'us-east-1',
            default_zone: 'us-east-1a',

            ssh_key_name: 'DEPLOYR-KEY',
            security_group_name: 'DEPLOYR-SECURITY-GROUP',

        },

    },

    scripts: {
        booted: '',
        uploaded: '',
        installed: '',
        deployed: '',
    }

}
