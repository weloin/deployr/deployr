import files from "../helpers/files.js";
import config from "../config.js";

export default {

    signature: 'aws:instance [instances]',
    description: 'Get info about aws instances.',

    async options(args){
        return args
            .positional('instances', {
                type: 'array',
            })
            .option('region', {
                alias: 'r',
                type: 'string',
                default: ''
            });
    },

    async handle(args){
        const path = files.absPath(config.dirs.core, 'Deployer/AwsInstanceList.js');
        const List = await files.load(path);
        const list = new List();

        await list.config(args);
        await list.show();
    },

}
