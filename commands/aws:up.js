import files from "../helpers/files.js";
import config from "../config.js";

export default {

    signature: 'aws:up [command]',
    description: 'Deploy application to AWS server.',

    async options(args){
        return args
            .positional('command', {
                type: 'string',
                default: '',
            })
            .option('region', {
                alias: 'r',
                type: 'string',
                default: ''
            })
            .option('zone', {
                alias: 'z',
                type: 'string',
                default: ''
            })
            .option('instance', {
                alias: 'i',
                type: 'string',
                default: '',
            })
            .option('remote-path', {
                alias: 'p',
                type: 'string',
                default: '~/',
            })
            .option('type', {
                alias: 't',
                type: 'string',
                default: config.deploy.aws.default_type || 't2.micro',
            })
            .option('number', {
                alias: 'n',
                type: 'integer',
                default: 1
            })
            .option('forever', {
                alias: 'pm2',
                type: 'boolean',
                default: false
            });
    },

    async handle(args){
        const path = files.absPath(config.dirs.core, 'Deployer/AwsDeployer.js');
        const Deployer = await files.load(path);
        const deployer = new Deployer();

        await deployer.config(args);
        await deployer.deploy();
    },

}
