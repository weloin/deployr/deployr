import config from "../config.js";
import files from "../helpers/files.js";

export default {

    signature: 'aws:regions',
    description: 'List all AWS Regions.',

    async options(args){
        return args.option('available', {
            type: 'boolean',
            default: false,
        });
    },

    async handle(args){

        const path = files.absPath(config.dirs.helpers, 'aws.js');
        const aws = await files.load(path);
        const DataTable = await files.load(files.absPath(config.dirs.core, 'DataTable.js'));

        DataTable.make(
            (await aws.getRegions(args.available))
                .sort((a, b) => a.RegionName.localeCompare(b.RegionName))
                .only(['RegionName', 'OptInStatus'])
        ).printTable();
    },

}
