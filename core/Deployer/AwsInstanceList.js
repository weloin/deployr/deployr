import config from "../../config.js";
import aws, {Instance} from "../../helpers/aws.js";
import DataTable from "../DataTable.js";
import log from "../../helpers/log.js";

export default class {

    options;


    /**
     * Configure the deployer first before deployment.
     *
     * @param options
     * @return {Promise<void>}
     */
    async config(options) {

        this.options = {
            instances: options.instances || [],
            region: options?.region || [ config.deploy.aws.default_region ],
            log_output: config.log?.output || 'app.out.log',
            log_errors: config.log?.error || config.log?.output || 'app.err.log',
        };

        if (typeof this.options.region === 'string') {

            if( this.options.region === 'all' ){
                this.options.region = (await aws.getRegions(true)).pluck('RegionName');
            } else {
                this.options.region = this.options.region.split(',');``
            }
        }

        log.debug('DEPLOYR:AWS:INFO:CONFIG', this.options);

    }


    /**
     * Deployment Initializer.
     *
     * @return {Promise<void>}
     */
    async show() {


        const result = await Promise.allSettled(
            this.options.region.map((region) => aws.findInstances(region, this.options.instances))
        );

        const instances = result
            .map(r => r.value)
            .reduce((o, n) => o.concat(n), [])
            .map(i => {
                const tags = Object.keys(i.tags);
                tags.forEach((tag) => {
                    i[`#${tag}`] = i.tags[tag]
                });
                delete i.tags
                return i;
            });

        DataTable.make(instances).printTable();

        process.exit();

    }

}
