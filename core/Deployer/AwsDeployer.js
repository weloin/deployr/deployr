import config from "../../config.js";
import aws, {Instance} from "../../helpers/aws.js";
import DataTable from "../DataTable.js";
import {ResourceType} from "@aws-sdk/client-ec2";
import log from "../../helpers/log.js";

export default class {

    options;


    /**
     * Configure the deployer first before deployment.
     *
     * @param options
     * @return {Promise<void>}
     */
    async config(options) {

        this.options = {
            instance: options.instance || null,
            region: options?.region || [ config.deploy.aws.default_region ],
            zone: options?.zone || config.deploy.aws.default_zone,
            type: options?.type || 't2.micro',
            command: options?.command || '',
            number: options?.number || 1,
            forever: options?.forever || false,
            remotePath: options?.remotePath || '~/',
            log_output: config.log?.output || 'app.out.log',
            log_errors: config.log?.error || config.log?.output || 'app.err.log',
        };

        if (typeof this.options.region === 'string') {

            if( this.options.region === 'all' ){
                this.options.region = (await aws.getRegions(true)).pluck('RegionName');
            } else {
                this.options.region = this.options.region.split(',');``
            }
        }

        log.debug('DEPLOYR:AWS:CONFIG', this.options);

    }


    /**
     * Deployment Initializer.
     *
     * @return {Promise<void>}
     */
    async deploy() {

        const deployed = await Promise.allSettled(
            this.options.region.map(async (region) => {
                return await this.startDeployment(region);
            })
        );

        const installed = deployed.filter(r => r.status === 'fulfilled');
        const failed = deployed.filter(r => r.status === 'rejected');

        log.info('DEPLOYR:AWS:DEPLOYED', {
            success: installed.length,
            failed: failed,
        });

        const instances = installed
            .map(r => r.value)
            .reduce((o, n) => o.concat(n), [])
            .map(i => {
                if( typeof i.tags !== 'undefined' ){
                    const tags = Object.keys(i.tags);
                    tags.forEach((tag) => {
                        i[`#${tag}`] = i.tags[tag]
                    });
                    delete i.tags
                }
                return i;
            });

        DataTable.make(instances).printTable();

        process.exit();

    }



    /**
     * Start the deployment.
     *
     * @param region
     * @return {Promise<array>}
     */
    async startDeployment(region) {


        log.info('DEPLOYR:AWS:START', {region});


        let instances = [];

        if( this.options.instance ){
            instances = await aws.findInstances(region, this.options.instance);
        }else{
            instances = await this.startInstances(region);
        }

        log.debug('DEPLOYR:AWS:INFO', {
            region,
            instances: instances.length,
        });


        const deployed = await Promise.allSettled(

            instances.map(async (instance) => {


                /**
                 * Wait until the instance has properly booted.
                 */

                await instance.waitUntilBooted();

                if( config.scripts.booted ){
                    await instance.exec( config.scripts.booted );
                }


                /**
                 * Upload Application, skip un-necessary files.
                 */

                await this.uploadFiles(region, instance);

                if( config.scripts.uploaded ){
                    await instance.exec( config.scripts.uploaded )
                }


                /**
                 * Install all project dependencies
                 */

                const install_response = await this.installDependencies(region, instance);

                if( config.scripts.installed ){
                    await instance.exec( config.scripts.installed );
                }


                /**
                 * Execute the requested command.
                 *
                 * NOTE: Ignoring 'await' because sometimes, the ssh command keeps running in the background & stalls the process.
                 */

                if( this.options.command ){
                    await this.runInitCommand(region, instance);
                    log.info('DEPLOYR:AWS:APP-STARTED', {region, instance});
                }


                if( config.scripts.deployed ){
                    await instance.exec( config.scripts.deployed );
                }

                return instance;

            })
        );

        return deployed
            .filter(i => i.status === 'fulfilled')
            .map(i => i.value)
            .only(['id', 'region', 'public_ip', 'state', 'type', 'ssh_key_name']);

    }


    /**
     * Start all required instances in a region.
     *
     * @param {string} region
     * @return {Promise<[any]>}
     */
    async startInstances(region) {

        /**
         * Create the SSH key-pair if there is none.
         */
        const deploy_key = await aws.createDeployKeyIfNotExists(region);
        log.debug('DEPLOYR:AWS:DEPLOY-KEY', {region, deploy_key});

        /**
         * Create a security group with access from everywhere,
         * if one in not already there.
         */
        const security_group = await aws.createDeploySecurityGroupIfNotExists(region);
        log.debug('DEPLOYR:AWS:SECURITY-GROUP', {region, security_group});


        /**
         * Get the supported ubuntu image for AWS using
         * the AMI locator.
         */
        const image = (await aws.ubuntuAMILocator(region)).find(im => im.version.match('22.04') && im.arch === 'amd64');
        log.debug('DEPLOYR:AWS:IMAGE', {region, image});

        /**
         * Start Instances based on the required parameters.
         */

        const tags = {
            Name: this.options.command?.toUpperCase() || config.app.name,
            Group: config.app.group,
        }

        const run_config = {
            ImageId: image.ami_id,
            InstanceType: this.options.type,
            MinCount: Math.max(this.options.number || 1, 1),
            MaxCount: Math.max(this.options.number || 1, 1),
            KeyName: deploy_key.name,
            AssociatePublicIpAddress: true,
            SecurityGroups: [security_group.GroupName],
        };

        if( this.options.zone ){
            run_config.Placement = {
                AvailabilityZone: this.options.zone,
            };
        }

        const tags_specifications = {
            ResourceType: ResourceType.instance,
            Tags: [],
        };

        Object.keys(tags).map(key => tags_specifications.Tags.push({Key: key, Value: tags[key]}));
        run_config.TagSpecifications = [ tags_specifications ];

        log.debug('DEPLOYR:AWS:RUN-INSTANCES-CONFIG', run_config);

        const instances = (await aws.ec2(region).runInstances(run_config)).Instances;


        /**
         * Return the instances as an object of Instance Class.
         */

        return instances.map(instance => new Instance(instance, region));

    }


    /**
     * Upload all application files.
     *
     * @param {string} region
     * @param {Instance} instance
     * @return {Promise<string>}
     */
    async uploadFiles(region, instance) {

        log.debug('DEPLOYR:AWS:UPLOAD-FILES', {region, instance});

        let excludes = config?.excludes || [];
        excludes = typeof excludes == 'string' ? [ excludes ] : excludes;

        const uploaded = await instance.upload(
            `${config.dirs.app}/`,
            this.options.remotePath,
            excludes
        );

        log.info('DEPLOYR:AWS:UPLOAD', {region, instance});

        return uploaded;

    }


    /**
     * Install all application dependencies.
     *
     * @param {string} region
     * @param {Instance} instance
     * @return {Promise<string>}
     */
    async installDependencies(region, instance) {

        /**
         * Update APT
         */
        log.debug('DEPLOYR:AWS:UPDATE-APT', {region, instance});
        await instance.exec('sudo apt-get update -y');



        /**
         * Install nvm, node and project dependencies.
         */
        log.debug('DEPLOYR:AWS:INSTALL-DEPENDENCIES', {region, instance});

        const install_cmds = [
            'sudo timedatectl set-ntp true',
            'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash',
            'source ~/.nvm/nvm.sh',
            'nvm install --lts',
            'nvm alias default node',
            'rm -rf ~/.nvm',
            'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash',
            'source ~/.nvm/nvm.sh',
            'nvm install --lts',
            'nvm alias default node',
            'npm i'
        ];


        if( this.options.forever ){
            install_cmds.push('npm i -g pm2');
        }

        log.debug('DEPLOYR:AWS:INSTALL-COMMANDS', {region, instance, install_cmds});

        const installed = await instance.exec(install_cmds);

        log.info('DEPLOYR:AWS:INSTALLED', {region, instance});

        return installed;

    }


    /**
     *
     * @param {string} region
     * @param {Instance} instance
     * @return {Promise<string>}
     */
    async runInitCommand(region, instance) {

        log.debug('DEPLOYR:AWS:STARTING-APP', {region, instance});
        const start_cmds = ['source ~/.nvm/nvm.sh'];

        if( this.options.forever ){
            start_cmds.push(`pm2 start -o ${this.options.log_output} -e ${this.options.log_errors} ${config.app.start_script} -- ${this.options.command}`);
        }else{
            start_cmds.push(`nohup node ${config.app.start_script} ${this.options.command} >> "${this.options.log_output}" 2>> "${this.options.log_errors}" < /dev/null &`);
        }

        log.debug('DEPLOYR:AWS:START-COMMANDS', start_cmds);

        return await instance.exec(start_cmds, {
            env: { APP_ENV: 'prod' },
        });

    }
}
