import {Table} from "console-table-printer";
import asciichart from "asciichart";
import utils from "../helpers/utils.js";

export default class DataTable {

    values;
    options;


    /**
     * @param values
     * @param {*} options
     */
    constructor(values, options = {}) {

        this.options = Object.assign({}, {
            timestamp_fields: [],
        }, options);

        this.values = values;
    }

    static make(...args){
        return new DataTable(...args);
    }



    printTable(options = {}, table_options = {}){
        console.log( this.renderTable(options, table_options) );
        return this;
    }


    /**
     * @param {*} options
     * @param {*} table_options
     */
    renderTable(options = {}, table_options = {}){

        options = Object.assign({}, {
            header: null,
            format: (item) => item,
            footer: (found) => `Found ${found.length} ${found.length > 1 ? 'items' : 'item'}.`,
        }, options);


        /**
         * Final results will be stored as an array of string.
         * @type {string[]}
         */
        const result = [];


        /**
         * Format Values.
         */
        const values = [...this.values].map((item) => {
            return this.options.timestamp_fields.reduce((item, field) => {
                if( typeof item[ field ] !== 'undefined' ){
                    item[ field ] = (new Date( item[ field ] )).toISOString();
                }
                return item;
            }, item);
        });

        /**
         * Add a header if exists.
         */
        if( options.header ){
            const header = typeof options.header === 'function' ? options.header(values): options.footer;
            result.push( header );
        }



        /**
         * Add the DataTable if values exists.
         */

        if( values.length ){
            const table = new Table(table_options);
            table.addRows(
                values
                    /**
                     * Map as per the format specifier.
                     */
                    .map(item => options.format({...item}))

                    /**
                     * Convert any object to string to properly display.
                     */
                    .map(item => item.objMap(v => (v && typeof v === 'object') ? JSON.stringify(v) : v))
            );
            result.push( table.render() );
        }


        /**
         * Add a footer if exists.
         */
        if( options.footer ){
            const footer = typeof options.footer === 'function' ? options.footer(values): options.footer;
            result.push( footer );
        }

        return result.join('\n');

    }

    printChart(options, plot_options = {}){
        console.log( this.renderChart(options, plot_options) );
        return this;
    }

    renderChart(options, plot_options = {}){

        options = Object.assign({}, {
            value: v => v,
            evaluate: c => utils.average(...c),
            max_width: 120,
        }, options);

        const chunks = utils.chunk(
            this.values.map(options.value),
            Math.ceil(this.values.length / options.max_width)
        );

        const values = chunks.map(options.evaluate);

        return asciichart.plot(values, plot_options);
    }


    offset(start){
        return new DataTable([...this.values].slice(start), this.options);
    }

    limit(length){

        if( length <= 0 ){
            return this;
        }

        return new DataTable([...this.values].slice(0, length), this.options);
    }

    reverse(length){
        return new DataTable([...this.values].reverse(), this.options);
    }

    orderBy(orderby){

        /**
         * Format Orderby Properly.
         */
        orderby = typeof orderby == 'string'
            ? orderby.replace( '\\,', '{/COMMA/}' ).split(',').map(v => v.replaceAll('{/COMMA/}', ','))
            : orderby;


        if( orderby ){

            const values = [...this.values].sort((a, b) => {

                for ( let field of orderby ){

                    let dir = 'asc';

                    if( field.substring(0, 1) === '-' ){
                        dir = 'desc';
                        field = field.substring(1);
                    }

                    if( a[ field ] === b[ field ]  ){
                        continue;
                    }

                    if( dir === 'asc' ){
                        if( typeof a[ field ] === 'string' || typeof b[ field ] === 'string' ){
                            return a[ field ].toString().localeCompare( b[ field ] );
                        }else{
                            return a[ field ] - b[ field ];
                        }
                    }

                    if( typeof a[ field ] === 'string' || typeof b[ field ] === 'string' ){
                        return b[ field ].toString().localeCompare( a[ field ] );
                    }else{
                        return b[ field ] - a[ field ];
                    }

                }

                return 0;

            });

            return new DataTable([...values], this.options);
        }

        return this;

    }

    where(where){

        let values = [...this.values];

        /**
         * Format Where Conditions Properly.
         */
        where = typeof where == 'string' ? where.split(',') : where;

        let conditions = [];
        if( where.length ) {

            conditions = where
                .filter(where => where.trim())
                .map((wh) => {

                    wh = wh.match(/\s*([a-z0-9_]+)\s*(=|<=|>=|%=|<|>)\s*(.*)/i);
                    wh = {
                        field: wh[1],
                        operator: wh[2],
                        value: wh[3],
                    };

                    if( this.options.timestamp_fields.indexOf( wh.field ) >= 0 ){
                        wh.value = typeof wh.value === 'string' ? Date.parse( wh.value ) : wh.value;
                    }

                    return wh;
                });
        }


        if( conditions.length ){
            values = values.filter((log) => {
                let found = true;

                for( const cond of conditions ){

                    const data = log[ cond.field ] || '';

                    switch( cond.operator ){
                        case '=':
                            found &&= data == cond.value;
                            break;

                        case '<':
                            // console.log(cond.field, log[ cond.field ], cond.value, log[ cond.field ] < cond.value);
                            found &&= data < cond.value;
                            break;

                        case '>':
                            found &&= data > cond.value;
                            break;

                        case '<=':
                            found &&= data <= cond.value;
                            break;

                        case '>=':
                            found &&= data >= cond.value;
                            break;

                        case '%=':
                            found &&= data.match(new RegExp(cond.value, 'i'));
                            break;
                    }
                }

                return found;
            });
        }

        return new DataTable(values, this.options);

    }

    pipe(callback) {
        if (typeof callback === 'function') {
            callback(this.values, this);
        }
        return this;
    }

}
