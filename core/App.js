import _yargs from 'yargs/yargs';
import { hideBin } from 'yargs/helpers';
import config from "../config.js";
import env from "../helpers/env.js";
import files from "../helpers/files.js";
import globalCommandOptions from "../helpers/globalCommandOptions.js";
import log from "../helpers/log.js";
import fs from "fs";
import utils from "../helpers/utils.js";

export default class {

    static make(...args){
        return new this(...args);
    }

    async start() {
        await this.init();
        await this.run();
    }

    async init() {
        await this.initCommands();
        return this;
    }

    async run(){
        return this;
    }

    async initCommands() {

        const commands = await files.loadAll(files.absPath(config.dirs.commands, '*.js'));
        const yargs = _yargs( hideBin( process.argv ) );

        for (const cmd of commands) {
            try{
                yargs.command(
                    cmd.signature,
                    cmd.description || '',
                    async (...args) => {
                        await globalCommandOptions.options(...args);
                        if( typeof cmd.options === 'function' ){
                            cmd.options(...args);
                        }
                    },
                    async (...args) => {
                        try{
                            /**
                             * Global Handler
                             */
                            await globalCommandOptions.handle(...args);

                            /**
                             * Command Handler
                             */
                            await cmd.handle(...args);

                        }catch (err) {
                            console.log(err);
                            log.error('DEPLOYR:COMMAND-EXECUTION-FAILED', err);
                        }
                    },
                );
            }catch (err) {
                log.error('DEPLOYR:COMMAND-LOAD-FAILED', err);
            }
        }

        return yargs
            .demandCommand(1, '')
            .wrap(null)
            .parse();
    }


    static async loadConfig() {

        let json_files = await files.findAll( `${config.dirs.app}/deployr.json` );

        await Promise.all( env.all().map(async (env) => {
            json_files = json_files.concat(
                await files.findAll( `${config.dirs.app}/deployr.${env}.json` )
            );
        }));

        const configs = json_files.map( file => JSON.parse( fs.readFileSync( file ).toString() ) );
        utils.mergeDeep(config, ...configs);

    }
}
