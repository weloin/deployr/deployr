Array.prototype.only = function(attrs) {
    return this.map((item) => {
        const value = {};
        attrs.map((attr) => item[ attr ] ? value[ attr ] = item[ attr ] : null);
        return value;
    })
};

Array.prototype.groupBy = function(attr) {
    const values = {};
    this.forEach((item) => {
        const group_name = item[ attr ];
        if( typeof values[ group_name ] === 'undefined' ){
            values[ group_name ] = [];
        }
        values[ group_name ].push(item);
    })
    return values;
};

Array.prototype.pluck = function(value, key = null) {
    if( key ){
        const values = {};
        this.map((item) => values[ item[ key ] ] = item[ value ]);
        return values;
    }
    return this.map((item) => item[ value ]);
};

Array.prototype.first = function() {
    return this.length ? this[0] : undefined;
};

// TODO: Group by & object based mapping.
Object.prototype.objMap = function ( method ){
    const values = {};
    Object.keys( this ).forEach((key) => {
        const value = this[ key ];
        if( typeof value === 'function' ){
            values[ key ] = this[ key ];
        }else{
            values[ key ] = method( this[ key ], key, this );
        }
    });
    return values;
}

Object.prototype.toObjectArray = function (key = 'key', value = 'value'){
    const values = [];
    Object.keys(this).map((obj_key) => {
        values.push({
            [key]: obj_key,
            [value]: this[ obj_key ],
        });
    });
    return values;
}
