import glob from "glob";

export default {


    unslashit(path){
        return path.replace(/^\/+/, '').replace(/\/+$/, '');
    },


    /**
     * @param {...string} paths
     * @return {string}
     */
    absPath(...paths){

        if( Array.isArray(paths[0]) ){
            paths = paths[0];
        }

        let abs = global.APP_ROOT;
        for (const p of paths){
            if( ! p ) continue;
            abs += '/' + this.unslashit(p);
        }

        return abs;
    },

    async findAll(path_pattern){
        return await glob(path_pattern);
    },

    async load(path, apply = (module) => module.default){
        if( typeof path === 'string' ){
            path = [path];
        }
        return await import( path ).then(apply);
    },

    async loadAll(path_pattern, apply = (module) => module.default) {
        const files = await this.findAll(path_pattern, apply);
        return await Promise.all(files.map(async file => await this.load(file)));
    }
}
