import utils from "./utils.js";
import App from "../core/App.js";
import log from "./log.js";
import fs from "fs";

export default {

    app_meta: JSON.parse( fs.readFileSync( 'package.json' ).toString() ),

    async merge(envs, reload_config = true) {

        if (typeof envs === 'string') {
            envs = envs.split(',').map( str => str.trim() );
        }

        if (!Array.isArray(envs)) {
            throw {message: 'Invalid Environment Override!'};
        }

        if( envs.length === 0 ){
            return;
        }

        envs = utils.uniqueValues(this.all().concat(envs));
        global.APP_ENV = envs;

        if( reload_config ){
            await App.loadConfig();
        }

        log.debug('DEPLOYR:ENV', envs.join(', '));

        return envs;

    },

    all(){
        return Array.isArray(global.APP_ENV) ? global.APP_ENV : [ global.APP_ENV ];
    },

    is(env){
        return this.all().includes(env);
    },

}
