import {EC2} from "@aws-sdk/client-ec2";
import config from "../config.js";
import files from "./files.js";
import fs from "fs";
import path from "path";
import axios from "axios";
import shell from "./shell.js";
import utils from "./utils.js";
import log from "./log.js";


const aws = config.deploy.aws;


export class Instance {

    id;
    public_ip;
    state_code;
    state;
    type;
    ami_id;
    public_dns;
    ssh_key_name;
    // launch_index;
    launched_at;
    tags;
    region;
    login_user = 'ubuntu';


    static async me(){
        const identity = (await axios.get('http://169.254.169.254/latest/dynamic/instance-identity/document')).data || null;
        if( ! identity ) return null;

        const instance_id = (await axios.get('http://169.254.169.254/latest/meta-data/instance-id')).data || null;
        if( ! instance_id ) return null;

        return await Instance.get(identity.region, instance_id);
    }

    static async get(region, instance_id) {

        const instances = (await Aws.ec2(region).describeInstances({
            InstanceIds: Array.isArray(instance_id) ? instance_id : [instance_id],
        }))
            ?.Reservations?.first()
            ?.Instances?.map(instance => new Instance(instance, region));

        if (Array.isArray(instance_id)) {
            return instances;
        }

        return instances.first();

    }

    constructor(instance, region) {
        this.set(instance, region);
    }

    set(instance, region = null) {

        this.id = instance?.InstanceId;
        this.public_ip = instance?.PublicIpAddress;
        this.public_dns = instance?.PublicDnsName;
        this.state_code = instance?.State?.Code;
        this.state = instance?.State?.Name;
        this.type = instance?.InstanceType;
        this.ami_id = instance?.ImageId;
        this.ssh_key_name = instance?.KeyName;
        // this.launch_index = instance?.AmiLaunchIndex;
        this.launched_at = instance?.LaunchTime;

        this.type = instance?.InstanceType;

        this.tags = instance.Tags?.reduce((tags, tag) => {
            tags[tag.Key.toLowerCase()] = tag.Value;
            return tags;
        }, {});

        if (region) {
            this.region = region;
        }
    }


    async waitUntilBooted() {

        log.debug('DEPLOYR:AWS:INSTANCE:BOOT-WAIT', this);

        while ( ! this.isRunning() ) {
            await utils.wait(5 * 1000);
            await this.refresh();
        }

        log.debug('DEPLOYR:AWS:INSTANCE:BOOTING', this.id);

        while (true) {

            try {
                await this.exec('ls');
                break;
            } catch (e) {
            }

            await utils.wait(10 * 1000);
        }

        await utils.wait(10 * 1000);

        log.info('DEPLOYR:AWS:INSTANCE:BOOTED', this.id);

    }

    async refresh() {

        const instance = (await Aws.ec2(this.region).describeInstances({
            InstanceIds: [this.id],
        }))?.Reservations?.first().Instances?.first();

        if (!instance) {
            return false;
        }

        this.set(instance);

        return true;
    }

    remote() {
        return `${this.login_user}@${this.public_ip}`;
    }

    isRunning() {
        return this.state === 'running';
    }

    async hasBooted() {
        // TODO: use ssh login to check if it has booted.
    }


    /**
     * @param {string|string[]} command A single command or multiple commands to be executed one after one. If, one fails, it will not proceed further.
     * @param {} options
     * @param {function(string)|null} options.onMessage
     * @param {function(string)|null} options.onError
     * @param {string|null} options.env
     * @return {Promise<string>}
     */
    async exec(command, options = {}) {
        const ssh_key_path = await Aws.keyPath(this.region);

        if( Array.isArray( command ) ){
            command = command.join(' && ');
        }

        const ssh_cmd = `ssh -i "${ssh_key_path}" -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null ${this.remote()} "source /etc/profile && ${shell.escape(command)}"`;
        const res = await shell.exec(ssh_cmd, options).catch(e => log.error(e))

        log.debug('CMD-RESPONSE', `\n--------------------------------\n${res}\n--------------------------------\n`);

        return res;
    }


    /**
     * @param {string|null} local_abs_path
     * @param {string|null} remote_abs_path
     * @param {[string]} excludes
     * @return {Promise<*>}
     */
    async upload(local_abs_path = null, remote_abs_path = null, excludes = null) {

        if (!local_abs_path) {
            local_abs_path = files.absPath(config.dirs.root);
        }

        const ssh_key_path = await Aws.keyPath(this.region);

        return await shell.upload(this.remote(), local_abs_path, remote_abs_path, ssh_key_path, excludes);
    }

    async terminate(){
        return await Aws.ec2(this.region).terminateInstances({
            InstanceIds: [this.id],
        })
    }


}


const Aws = {

    ec2(region = aws.default_region) {
        return new EC2({
            version: aws.api_version,
            region: region,
            credentials: {
                accessKeyId: aws.api_key,
                secretAccessKey: aws.api_secret,
            }
        });
    },

    async getRegions(available = false) {

        const params = {
            AllRegions: true,
        }


        if (available) {
            params.Filters = [
                {
                    Name: 'opt-in-status',
                    Values: [
                        'opt-in-not-required',
                        'opted-in'
                    ]
                }
            ];
        }

        return (await this.ec2()
            .describeRegions(params))?.Regions || [];
    },

    async keyNameDepreciated(region = aws.default_region) {
        return `${region}-${aws.ssh_key_name}.key`;
    },

    async keyPathDepreciated(region = aws.default_region) {
        return files.absPath(config.dirs.storage, 'keys', await this.keyName(region));
    },

    async getDeployKey(region = aws.default_region) {
        return (await this.ec2(region).describeKeyPairs({
            KeyNames: [await this.keyName(region)],
        }))?.KeyPairs?.first();
    },

    async getVpcs(region = aws.default_region) {
        return (await this.ec2(region).describeVpcs({}))?.Vpcs;
    },

    async keyName(region = aws.default_region) {
        return aws.ssh_key_name;
    },

    async keyPath(region = aws.default_region) {
        return [
            config.dirs.storage,
            'keys',
            await this.keyName(region)
        ].join('/');
    },


    /**
     * @param region
     * @return {Promise<{private: string, public: string, name: string}>}
     */
    async createDeployKeyIfNotExists(region) {

        const name = await this.keyName(region);
        const key_path = await this.keyPath(region);
        const public_path = `${key_path}.pub`;


        /**
         * If a deployment key does not exist, then generate
         * a new key-pair.
         */

        if (!fs.existsSync(key_path) || !fs.existsSync(key_path)) {
            log.info('DEPLOYR:AWS:DEPLOY-KEY:GENERATE', {id: this.id, key_path});
            await utils.generateKeyPair(key_path);
        }


        try {

            /**
             * Check if the deployment key-pair exists in AWS,
             * if exists, then delete the key-pair before importing
             * to AWS Instance.
             */

            const exists = await this.getDeployKey(region);

            await this.ec2(region).deleteKeyPair({
                KeyName: name,
            });

        } catch (e) {}

        const public_key_content = fs.readFileSync(public_path).toString();

        /**
         * Import the public key to AWS Instance.
         */

        try{
            await this.ec2(region).importKeyPair({
                KeyName: name,
                PublicKeyMaterial: Buffer.from(public_key_content),
            });
        }catch (e) {
            log.warn('DEPLOYR:AWS:DEPLOY-KEY:IMPORT-ERROR', e);
            throw e;
        }


        return {
            name,
            public: public_path,
            private: key_path,
        }

    },

    async createDeployKeyIfNotExistsDepreciated(region = aws.default_region) {

        let deploy_key = null;
        const key_path = await this.keyPath(region);
        const key_exists = fs.existsSync(key_path);

        try {
            deploy_key = await this.getDeployKey(region);
            log.debug('DEPLOYR:AWS:DEPLOY-KEY', {id: this.id, deploy_key});
        } catch (e) {
            log.warn('DEPLOYR:AWS:DEPLOY-KEY:NOT-FOUND', {id: this.id, e});
        }


        if (!deploy_key || !key_exists) {

            /**
             * Delete The deploy-key if already-exists.
             */
            if (deploy_key) {
                await this.ec2(region).deleteKeyPair({
                    KeyName: await this.keyName(region),
                });
            }


            /**
             * Create a new SSH key-pair in AWS.
             */
            deploy_key = await this.ec2(region).createKeyPair({
                KeyName: await this.keyName(region),
            });

            log.debug('DEPLOYR:AWS:DEPLOY-KEY', {id: this.id, deploy_key});


            /**
             * Create the directory if not exists.
             */
            fs.mkdirSync(path.dirname(key_path), {recursive: true});


            /**
             * Save the private SSH key to the file.
             */
            fs.writeFileSync(key_path, deploy_key.KeyMaterial);
            fs.writeFileSync(`${key_path}.fingerprint`, deploy_key.KeyFingerprint);


            /**
             * Change the permission to make sure only the owner can
             * modify, no one else. This is required for SSH logins,
             * else it will throw permission errors.
             */
            fs.chmodSync(key_path, 0o600);

        }


        return {
            id: deploy_key.KeyPairId,
            path: key_path,
            name: deploy_key.KeyName,
            contents: deploy_key.KeyMaterial,
            fingerprint: deploy_key.KeyFingerprint,
        };

    },


    async createDeploySecurityGroupIfNotExists(region = aws.default_region) {

        let group = null;

        const vpc = (await this.getVpcs(region))?.first();

        try {
            group = (await this.ec2(region).describeSecurityGroups({
                GroupNames: [aws.security_group_name]
            }))?.SecurityGroups?.first();

            log.debug('DEPLOYR:AWS:SECURITY-GROUP:EXISTS', {id: this.id, group});

        } catch (e) {
        }


        if (!group) {

            /**
             * Action=CreateSecurityGroup
             * GroupDescription=All Access
             * GroupName=TEST-SECURITY-GROUP
             * Version=2016-11-15
             * VpcId=vpc-04c23862e83914a6c
             */

            const group_id = (await this.ec2(region).createSecurityGroup({
                GroupName: aws.security_group_name,
                Description: aws.security_group_name,
                VpcId: vpc.VpcId,
            }))?.GroupId;

            group = (await this.ec2(region).describeSecurityGroups({
                GroupIds: [group_id]
            }))?.SecurityGroups?.first();


            /**
             * Action=AuthorizeSecurityGroupIngress
             * GroupId=sg-0aaaff504becd4129
             * IpPermissions.1.FromPort=0
             * IpPermissions.1.IpProtocol=tcp
             * IpPermissions.1.IpRanges.1.CidrIp=0.0.0.0/0
             * IpPermissions.1.ToPort=65535
             * Version=2016-11-15
             */

            try {
                await this.ec2(region).authorizeSecurityGroupIngress({
                    GroupId: group.GroupId,
                    VpcId: group.VpcId,
                    IpPermissions: [{
                        FromPort: 0,
                        ToPort: 65535,
                        IpProtocol: 'tcp',
                        IpRanges: [{
                            CidrIp: '0.0.0.0/0'
                        }]
                    }]
                });
            } catch (e) {

            }

            log.debug('DEPLOYR:AWS:SECURITY-GROUP:CREATED', {id: this.id, group});

        }

        return group;

    },

    async ubuntuAMILocator(region) {
        const res = await axios.get('https://cloud-images.ubuntu.com/locator/ec2/releasesTable');
        // const images = JSON.parse(res.data.replace(/\,(?!\s*?[\{\[\"\'\w])/g, ''))?.aaData || [];
        const images = res.data?.aaData || [];

        return images
            .filter(img => img[0] === region)
            .map(img => ({
                region: img[0],
                name: img[1],
                version: img[2],
                arch: img[3],
                instance_type: img[4],
                release: img[5],
                ami_id: img[6].match(/ami-[0-9a-z]+/)?.at(0),
                aki_id: img[7],
            }));
    },

    async getAMImages(region) {

        const regex = new RegExp('(?=.*22\.04)(?=.*ubuntu)', 'i');

        return (await this.ec2(region).describeImages({
            MaxResults: 1000,
            // Owner: ['amazon'],
            Filters: [
                {Name: 'architecture', Values: ['x86_64']},
                {Name: 'image-type', Values: ['machine']},
                // {Name: 'hypervisor', Values: ['xen']},
                // {Name: 'state', Values: 'available'},
                // {Name: 'is-public', Values: [false]},
                // {Name: 'platform', Values: ['ubuntu']},
            ]
        }))?.Images?.filter(i => (i.Name?.match(regex) || i.Description?.match(regex)));
    },


    async findInstances(region, instance_ids) {

        if( typeof instance_ids === 'string' ){
            instance_ids = [ instance_ids ];
        }

        try{

            const params = {};

            if( instance_ids && (Array.isArray(instance_ids) && instance_ids.length)){
                params.InstanceIds = instance_ids
            }

            const data = (await this.ec2(region).describeInstances(params));

            return data.Reservations.reduce((all, reservation) => {
                return all.concat(reservation?.Instances?.map(instance => new Instance(instance, region)) ?? [])
            }, [])

        }catch (e) {
            log.error('DEPLOYR:AWS:FAILED', e.message);
        }

        return [];

    }

}


export default Aws;
