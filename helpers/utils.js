import axios from "axios";
import crypto from "node:crypto";
import fs from "fs";
import shell from "./shell.js";
import path from "path";
import log from "./log.js";

export default {


    isErrorObject(obj){
        return Object.prototype.toString.call(obj) === "[object Error]";
    },

    dottify(value, prefix = ''){

        if( typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean' || (value === null) ){
            return prefix ? {[prefix]: value} : value;
        }

        let final = {};
        for (const key in value){
            const dotted = this.dottify(value[key], prefix ? `${prefix}.${key}` : key);
            final = {...final, ...dotted};
        }
        return final;
    },


    toArray(thing){
        const val = [];

        if( Array.isArray( thing ) ){
            return thing;
        } else if (typeof thing === 'number' || typeof thing === 'string' || typeof thing === 'boolean') {
            val.push(thing);
        } else if( thing !== null && typeof thing === 'object' ){
            for( const key of Object.keys(thing) ){
                val.push({key, value: thing[key]});
            }
        }

        return val;
    },

    /**
     * Wait for a specified timestamp.
     *
     * @param ms
     * @return {Promise<void>}
     */
    wait(ms){
        return new Promise((resolve) => {

            // Resolve immediately if the waiting ms is 0 or below.
            if( ms <= 0 ){
                return resolve();
            }

            setTimeout(resolve, ms);
        })
    },


    /**
     * Retry an action to specified times.
     * @param {function} action
     * @param {int} times
     * @param {int} wait
     * @return {Promise<*>}
     */
    async retry(times, action, wait = 0){

        if( times <= 0 ) throw {message: 'Must try at least once.'};

        try{
            return await action(times);
        }catch (err) {
            if( times <= 1 ){
                log.critical('DEPLOYR:RETRY:PERMANENT-FAILURE', err);
                throw err;
            }else{
                log.warn('DEPLOYR:RETRY:FAILED', err);
            }
        }

        if( wait > 0 ){
            await this.wait(wait);
        }

        return await this.retry(times - 1, action);
    },


    /**
     * Repeat an action specified times.
     * @param {number} times
     * @param {function} action
     * @return {Promise<*>}
     */
    async repeat(times, action){

        let force_stop = false;

        const stop = () => {
            force_stop = true;
        }

        if( times < 0 ){
            while (1){
                if( force_stop ) break;
                await action({idx: times, times, stop});
            }

        }else{
            for (let idx = 0; idx < times; idx++){
                if( force_stop ) break;
                await action({idx, times, stop});
            }
        }

    },

    /**
     * Simple object check.
     * @param item
     * @returns {boolean}
     */
    isObject(item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    },

    /**
     * Deep merge two objects.
     * @param target
     * @param sources
     */
    mergeDeep(target, ...sources) {

        if (!sources.length) return target;
        const source = sources.shift();

        if (this.isObject(target) && this.isObject(source)) {
            for (const key in source) {
                if (this.isObject(source[key])) {
                    if (!target[key]) Object.assign(target, { [key]: {} });
                    this.mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(target, { [key]: source[key] });
                }
            }
        }

        return this.mergeDeep(target, ...sources);
    },


    /**
     * @param {Array} arr
     */
    arrayRand(arr){
        return arr[ Math.floor( Math.random() * arr.length ) ];
    },

    chunk(arr, size){
        return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
            arr.slice(i * size, i * size + size)
        );
    },

    min( ...values ){
        if( values.length && Array.isArray(values[0]) ){
            values = values[0];
        }
        return values.reduce( ( o, v ) => v < o ? v : o, Number.POSITIVE_INFINITY);
    },

    max( ...values ){
        if( values.length && Array.isArray(values[0]) ){
            values = values[0];
        }
        return values.reduce( ( o, v ) => v > o ? v : o, Number.NEGATIVE_INFINITY);
    },

    average( ...values ){
        if( values.length && Array.isArray(values[0]) ){
            values = values[0];
        }
        return values.reduce( ( p, c ) => p + c, 0 ) / values.length
    },

    median( ...values ){
        if( values.length && Array.isArray(values[0]) ){
            values = values[0];
        }

        if(values.length === 0){
            return null;
        }

        values = [...values].sort(function(a,b){
            return a-b;
        });

        const half = Math.floor(values.length / 2);

        if (values.length % 2)
            return values[half];

        return (values[half - 1] + values[half]) / 2.0;

    },


    findKeywords(content, keywords) {

        content = content?.toLowerCase();

        if( typeof keywords === 'string' ){
            keywords = [ keywords ];
        }

        return keywords.map((keyword) => {
            return content.match(keyword) ? keyword : null;
        }).filter(k => k);

    },

    async regions() {
        // const templates = await files.find('*.js', config.paths.aws_templates);
        // return await files.loadAll(templates);
    },

    async regionsNames() {
        return (await this.regions()).map(r => r.region);
    },

    /**
     * @return {Promise<string>}
     */
    async externalIp(){
        const res = await axios.get('http://ip-api.com/json/', {
            timeout: 5000,
            headers: {
                'content-type': 'application/json',
            }
        });

        return res.data?.query || 'unknown';
    },


    uniqueValues(arr){
        return [...new Set(arr)];
    },


    async generateKeyPair( key_path ){

        const dir = path.dirname( key_path );

        if( dir && ! fs.existsSync( dir ) ){
            fs.mkdirSync(dir, {recursive: true});
        }

        const public_path = `${key_path}.pub`;

        if( fs.existsSync( public_path ) ){
            fs.unlinkSync( public_path );
        }

        if( fs.existsSync( key_path ) ){
            fs.unlinkSync( key_path );
        }

        /**
         * Generate SSH Key-Pair
         */
        await shell.exec(`ssh-keygen -f "${ shell.escape( key_path ) }" -t rsa -b 4096 -N "" -q`);

        /**
         * Change the permission to make sure only the owner can
         * modify, no one else. This is required for SSH logins,
         * else it will throw permission errors.
         */
        await fs.chmodSync(key_path, 0o600);

        return {
            public: public_path,
            private: key_path,
        };
    }

};
