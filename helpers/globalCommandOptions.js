import env from "./env.js";
import files from "./files.js";
import config from "../config.js";

export default{

    async options(args){
        return args
            .option('env', {
                describe: 'Append new environment(s) for the application dynamically.',
                type: 'array',
                default: [],
            });
    },

    async handle(args) {

        if (args.env) {
            await env.merge(args.env);
        }

        if (args.ui) {
            const UiServer = await files.load( files.absPath(config.dirs.core, 'UiServer.js') );
            UiServer.start();
        }
    }

}
