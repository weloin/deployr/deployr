import config from "../config.js";
import files from "./files.js";
import colors from "@jshor/colors/safe.js";
import util from "util";
import utils from "./utils.js";

export const LEVEL_NAMES = {
    DEBUG:      'DEBUG',
    INFO:       'INFO',
    WARNING:    'WARNING',
    ERROR:      'ERROR',
    CRITICAL:   'CRITICAL',
};

export const LEVEL_VALUES = {
    DEBUG:      100,
    INFO:       200,
    WARNING:    300,
    ERROR:      400,
    CRITICAL:   500,
};

const logger = {

    paths: {
        dir: files.absPath(config.dirs.storage, 'logs'),
        file: files.absPath(config.dirs.storage, 'logs', 'bot.log')
    },

    max_level_length: Math.max( ...LEVEL_NAMES.toObjectArray().pluck('value').map( i => i.length ) ),

    log(level, message, context){

        const logging_level = LEVEL_VALUES[ config.log.level ] || LEVEL_VALUES.DEBUG;
        const level_value = LEVEL_VALUES[ level ];

        // Skip logging if the level is below logging level.
        if( level_value < logging_level ){
            return;
        }

        level = level.padEnd( this.max_level_length, ' ') + ':';

        if( level_value >= LEVEL_VALUES.CRITICAL ){
            level = colors.bgRed(level);
            message = colors.bgRed(message);

        }else if( level_value >= LEVEL_VALUES.ERROR ){
            level = colors.red(level);
            message = colors.red(message);

        }else if( level_value >= LEVEL_VALUES.WARNING ){
            level = colors.yellow(level);
            message = colors.yellow(message);

        }else if( level_value >= LEVEL_VALUES.INFO ){
            level = colors.blue(level);
            message = colors.blue(message);
        }else {
            level = colors.gray(level);
            message = colors.gray(message);
        }

        const line = [
            level,
            (new Date).toISOString(),
            '-',
            message,
        ];

        if( typeof context !== 'undefined' ){
            try{
                if(context && (Array.isArray(context) || typeof context === 'object')){

                    if( utils.isErrorObject( context ) ){
                        const ctx = {};
                        Object.getOwnPropertyNames(context).map((key) => {
                            ctx[ key ] = context[key];
                        });
                        context = ctx;
                    }

                    context = JSON.stringify(context);

                }else if( typeof context?.toString === 'function' ){
                    context = context.toString();
                }
            }catch (e) {
                context = JSON.stringify({
                    INSPECT: true,
                    data: util.inspect(context, {showHidden: true, depth: 2, color: true, breakLength: Infinity})
                });
            }

            if( level_value >= LEVEL_VALUES.ERROR ){
                context = colors.red(context);
            }

            line.push('---', context);
        }

        console.log( line.join(' ') );
    },

    debug:      (...args) => logger.log(LEVEL_NAMES.DEBUG,    ...args),
    info:       (...args) => logger.log(LEVEL_NAMES.INFO,     ...args),
    warn:       (...args) => logger.log(LEVEL_NAMES.WARNING,  ...args),
    error:      (...args) => logger.log(LEVEL_NAMES.ERROR,    ...args),
    critical:   (...args) => logger.log(LEVEL_NAMES.CRITICAL, ...args),

}

export default logger;
