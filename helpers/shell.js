import {spawn} from "node:child_process";
import log from "./log.js";

export default {


    /**
     * Escape command parts which is inside double quotes.
     *
     * @param chars
     * @return {string}
     */
    escape(chars){
        return chars
            .replaceAll('\\', '\\\\')
            .replaceAll('"', '\\"');
    },


    /**
     * Execute a shell command.
     *
     * @param command
     * @param {{onMessage:function, onError:function}} options
     * @param {number} options.timeout
     * @param {function(string)|null} options.onMessage
     * @param {function(string)|null} options.onError
     * @param {{any}|null} options.env
     * @return {Promise<string>}
     */
    async exec(command, options = {}) {

        options = Object.assign({}, {
            onMessage: null,
            onError: null,
            env: null,
            timeout: 0,
        }, options);


        if( Array.isArray( command ) ){
            command = command.join(' && ');
        }


        const exec_env = {...process.env, ...options.env};

        log.debug('DEPLOYR:SHELL:EXEC', command);

        return await new Promise((resolve, reject) => {

            const cmd = spawn(command, {
                env: exec_env,
                shell: true,
            });

            const kill_process = () => {
                if( cmd?.pid && ! cmd.killed ){
                    cmd.kill('SIGKILL');
                }
            };

            process.on('exit', kill_process);

            if( options.timeout > 0 ){
                setTimeout(kill_process, options.timeout * 1000);
            }


            let stdout = '';
            let stderr = '';

            cmd.stdout.on('data', (data) => {
                data = data.toString();
                if( typeof options.onMessage === 'function' ){
                    options.onMessage( data );
                }else{
                    stdout += data;
                }
            });

            cmd.stderr.on('data', (data) => {
                data = data.toString();
                if( typeof options.onError === 'function' ){
                    options.onError( data );
                }else{
                    stderr += data;
                }
            });

            cmd.on('close', function (code) {
                if( code ){
                    reject({command, code, message: stderr});
                }else{
                    resolve(stdout);
                }
                process.off('exit', kill_process);
            });

            cmd.on('error', (err) => {
                console.error('SUBPROCESS FAILED:', {command, err});
                process.off('exit', kill_process);
            });

        });
    },


    /**
     * Upload files to the remote server from local path.
     *
     * @param {string} remote
     * @param {string} local_abs_path
     * @param {string|null} remote_abs_path
     * @param {string|null} ssh_key_path
     * @param {string|string[]} excludes
     * @return {Promise<unknown>}
     */
    async upload(remote, local_abs_path, remote_abs_path = null, ssh_key_path = null, excludes = []){

        if( ! remote_abs_path ){
            remote_abs_path = '~/';
        }


        /**
         *  Add 'rsync' command to upload all files to server.
         */

        const cmd = [ 'rsync -azv' ];


        /**
         * Create remote directory if not exists.
         */
        if( remote_abs_path && remote_abs_path !== '~/' ){
            cmd.push(`--rsync-path="mkdir -p \\"${ this.escape( remote_abs_path ) }\\" && rsync"`);
        }


        /**
         * Set the ssh_key path if available and ignore any
         * previously used hosts checking to skip ssh-key
         * overwrite errors.
         */

        if( ssh_key_path ){
            cmd.push( `-e "ssh -i \\"${ this.escape( ssh_key_path ) }\\" -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null"` );
        }else {
            cmd.push( '-e "ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null"' );
        }


        /**
         * Prepare excludes.
         */

        if( typeof excludes === 'string' ){
            excludes = [ excludes ];
        }

        excludes = excludes.map(path => `--exclude="${ this.escape( path ) }"`).join(' ');

        if( excludes ){
            cmd.push(excludes);
        }



        /**
         * Add local & remote targets.
         */

        cmd.push(`"${local_abs_path}" ${remote}:${remote_abs_path}`);



        /**
         * Execute the command on shell & return the result.
         */

        return await this.exec( cmd.join(' ') );

    },

}
